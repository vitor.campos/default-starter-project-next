## Getting Started

Run development server on localhost:3000

```bash
   npm run dev
   # or
   yarn dev
```

Run api local to deal with mock data (CRUD) on localhost:3333

```bash
    npm run server
    # or
    yarn server
```

##

## Description

This is a default initial configuration of a new next/ts project!
