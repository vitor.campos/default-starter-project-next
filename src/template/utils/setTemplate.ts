interface IInitialTemplates {}

export const setTemplate: (
  path: string[],
  initialTemplates: IInitialTemplates
) => IInitialTemplates = (path, initialTemplates) => {
  switch (path[3]) {
    default:
      return initialTemplates;
  }
};
