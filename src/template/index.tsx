import { useRouter } from "next/router";
import { ReactNode, useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { setCurrentTheme } from "store/actions/ThemesActions";
import { usePersistentState } from "utils/usePersistentState";
import { setTemplate } from "./utils/setTemplate";

interface ITemplateProps {
  children: ReactNode;
}

export function Template({ children }: ITemplateProps) {
  const dispatch = useDispatch();
  const router = useRouter();

  const initialTemplates = {};
  const [theme, setTheme] = usePersistentState("@key/theme", "light");

  const [currentTemplate, setCurrentTemplate] = useState(initialTemplates);
  const [isLoading, setIsLoading] = useState(true);

  function setPath() {
    if (typeof window !== "undefined") {
      setIsLoading(true);
      const path = window.location.href.split("/");
      setCurrentTemplate(setTemplate(path, initialTemplates));
      setIsLoading(false);
    }
  }

  useEffect(() => {
    dispatch(setCurrentTheme(theme));
  }, [theme]);

  useEffect(() => {
    setPath();
  }, [router?.asPath]);

  if (isLoading) {
    return null;
  }

  return <>{children}</>;
}
