import React from "react";
import type { AppProps } from "next/app";

import { store } from "store";
import { Provider } from "react-redux";

import { ThemeProvider } from "styled-components";
import light from "styles/themes/light";
import dark from "styles/themes/dark";

import GlobalStyle from "styles/global";

import { Template } from "template";

function MyApp({ Component, pageProps }: AppProps) {
  const { currentTheme } = store.getState().ThemesReducer;

  const theme =
    currentTheme === "light" ? light : currentTheme === "dark" ? dark : light;

  return (
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <GlobalStyle />
        <Template>
          <Component {...pageProps} />
        </Template>
      </ThemeProvider>
    </Provider>
  );
}

export default MyApp;
