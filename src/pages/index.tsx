import type { NextPage } from "next";
import Head from "next/head";
import { Container } from "styles/homeStyles";

const Home: NextPage = () => {
  return (
    <Container>
      <Head>
        <title>Home Page</title>
        <meta name="Home" content="Hello world!" />
      </Head>

      <h1>Hello World!!</h1>
    </Container>
  );
};

export default Home;
