import styled from "styled-components";

export const Container = styled.div`
  background-color: ${(props) => props.theme.colors.primary};

  height: 100vh;
  width: 100vw;

  display: flex;
  align-items: center;
  justify-content: center;

  color: ${(props) => props.theme.colors.secondary};

  h1 {
    font-size: 10rem;
  }
`;
