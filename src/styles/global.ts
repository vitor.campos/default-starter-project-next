import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`
* {
  margin: 0;
  padding: 0;
  outline: transparent;
  box-sizing: border-box;
  
  /* font-family: */

  &::-webkit-scrollbar {
    width: 0.5rem;
    height: 0.5rem;
    background-color: #777;
    border-radius: 1rem;
  }
  &::-webkit-scrollbar-thumb {
    background-color: #ccc;
    border-radius: 1rem;
  }
}

html {
  font-size: 75%;
  @media screen and (min-width: 640px) {
    font-size: 81.25%;
  }
  
  @media screen and (min-width: 768px) {
    font-size: 87.5%;
  }
  
  @media screen and (min-width: 1024px) {
    font-size: 93.75%;
  }
  
  @media screen and (min-width: 1280px) {
    font-size: 100%;
  }
  
  @media screen and (min-width: 1536px) {
    font-size: 106.25%;
  }
  
  @media screen and (min-width: 1921px) {
    font-size: 112.50%;
  }
}

a {
  color: inherit;
  text-decoration: none;
}

ul, li, ol {
  list-style: none;
}

button {
  border: none;
}
`;
