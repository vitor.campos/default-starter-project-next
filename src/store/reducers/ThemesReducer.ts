interface IAction {
  type: string;
  payload: any;
}

interface IThemeReducer {
  currentTheme: string;
}

const initialState = {
  currentTheme: "",
};

export const CURRENT_THEME = "CURRENT_THEME";

export default function ThemesReducer(state = initialState, action: IAction) {
  const { type, payload } = action;

  switch (type) {
    case CURRENT_THEME:
      return { ...state, ...payload };
    default:
      return state;
  }
}
