import { combineReducers } from "redux";
import ThemesReducer from "./ThemesReducer";
export const Reducers = combineReducers({ ThemesReducer });

export type RootState = ReturnType<typeof Reducers>;
