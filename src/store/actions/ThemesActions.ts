import { CURRENT_THEME } from "store/reducers/ThemesReducer";

export function setCurrentTheme(payload: string) {
  return {
    type: CURRENT_THEME,
    payload,
  };
}
